<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd7e23fc79c1fb65e8a7a3c483f4f1861
{
    public static $prefixLengthsPsr4 = array (
        'B' => 
        array (
            'Battlemetrics\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Battlemetrics\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Battlemetrics\\Api' => __DIR__ . '/../..' . '/src/Api.php',
        'Battlemetrics\\Request' => __DIR__ . '/../..' . '/src/Request.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd7e23fc79c1fb65e8a7a3c483f4f1861::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd7e23fc79c1fb65e8a7a3c483f4f1861::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitd7e23fc79c1fb65e8a7a3c483f4f1861::$classMap;

        }, null, ClassLoader::class);
    }
}
