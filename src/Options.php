<?php

namespace Battlemetrics;

use ArrayIterator;
use IteratorAggregate;

class Options implements IteratorAggregate
{
    public static function create(iterable $options): Options
    {
        $self = new self;
        foreach ($options as $name => $option) {
            $self->{$name} = $option;
        }

        return $self;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this);
    }
}