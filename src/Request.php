<?php

namespace Battlemetrics;

use function curl_init;
use function curl_setopt;

class Request
{
    protected $endpoint = '';
    protected $options;
    protected $token = [];

    public static function create(string $endpoint): Request
    {
        $self = new self;
        $self->endpoint = preg_replace('/\/^/', '', $endpoint);

        return $self;
    }

    public function addOptions(iterable $options): Request
    {
        $this->options = $options;

        return $this;
    }

    public function addToken(string $token): Request
    {
        $this->token = $token;

        return $this;
    }

    public function getUrl(): string
    {
        return Api::URL . "$this->endpoint?" . http_build_query($this->options);
    }

    public function get(): Response
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->token,
            ]
        );
        $result = json_decode(curl_exec($curl), true);

        return new Response($result, $this->endpoint);
    }
}
