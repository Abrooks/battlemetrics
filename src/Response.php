<?php

namespace Battlemetrics;

class Response
{
    protected $response;

    public function __construct(array $result)
    {
        $this->loadResponse($result);
    }

    public function isError(): bool
    {
        if (isset($this->response['errors'])) {
            return true;
        }

        return false;
    }

    public function isSuccess(): bool
    {
        return !$this->isError();
    }

    public function getErrors(): array
    {
        if (isset($this->response['errors'])) {
            return $this->response['errors'];
        }

        return [];
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function loadResponse(array $response): void
    {
        foreach ($response as $key => $value) {
            $this->response[$key] = $value;
        }
    }
}
