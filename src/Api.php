<?php

namespace Battlemetrics;

use Generator;

class Api
{
    protected $token = '';
    protected $requests = [];
    protected $responses = [];
    const URL = 'https://api.battlemetrics.com';

    public static function create(string $token): Api
    {
        $self = new self;
        $self->token = $token;

        return $self;
    }

    public function addRequests(array $requests): Api
    {
        foreach ($requests as $request) {
            $request->addToken($this->token);
        }
        $this->requests = array_merge($this->requests, $requests);

        return $this;
    }

    public function send($forceRefresh = false): array
    {
        $responses = [];

        foreach ($this->requests as $name => $request) {
            $responses[$name] = $request->get();

            if (isset($this->responses[$name]) && !$forceRefresh) {
                $responses[$name] = $this->responses[$name];
            }
        }
        $this->responses = $responses;

        return $responses;
    }

    public function sendLazy(): Generator
    {
        foreach ($this->requests as $request) {
            yield $request->get();
        }
    }
}
