<?php

use Battlemetrics\Api;
use Battlemetrics\Options;
use Battlemetrics\Request;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$options = parse_ini_file('testing.ini');

$server1 = '3762584';
$requests = Api::create($options['token'])
               ->addRequests([
                   'server1' => Request::create("/servers/$server1/player-count-history")->addOptions(
                       Options::create([
                           'start' => '2020-03-01T12:00:00Z',
                           'stop'  => '2020-03-07T12:00:00Z',
                       ])
                   ),
                   Request::create('/players')->addOptions(
                       Options::create([
                           'filter' => [
                               'servers' => $server1,
                           ],
                       ])
                   )
               ])
               ->addRequests([
                   Request::create('/players')->addOptions(
                       Options::create([
                           'filter' => [
                               'servers' => $server1,
                           ],
                       ])
                   )
               ])->send();

foreach ($requests as $requestName => $request) {
    if ($request->isError()) {
        print_r($request->getErrors());
    } else {
        print_r($request->getResponse());
    }
}
